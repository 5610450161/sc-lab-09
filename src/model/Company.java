package model;

import interfaces.Taxable;

public class Company implements Taxable, Comparable {
	String name;
	double revenue;
	double expenditure;
	public Company(String name, double revenue, double expenditure){
		this.name = name;
		this.revenue = revenue;
		this.expenditure = expenditure;
	}
	@Override
	public double getTax() {
		double tax = 0;
		if (revenue - expenditure >= 0){
			tax = (revenue - expenditure) * 0.3;
		}
		// TODO Auto-generated method stub
		return tax;
	}
	@Override
	public int compareTo(Object obj) {
		// TODO Auto-generated method stub
		Company other = (Company)obj;
		return 0;
	}
}
