package model;


import interfaces.Taxable;

public class TaxCalculator {
	public static double sum(Taxable[] taxList){
		double sum = 0;
		for (Taxable t: taxList){
			sum = sum + t.getTax();
		}
		return sum;
		
	}
}
