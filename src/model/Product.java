package model;

import interfaces.Taxable;

public class Product implements Taxable, Comparable {
	String name;
	double price;
	public Product(String name, double price){
		this.name = name;
		this.price = price;
	}
	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		return price * 0.07;
	}
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		Product other = (Product)o; 
		if(this.price < other.price){
			return -1;
		}else if(this.price > other.price){
			return 1;
		}
		return 0;
	}
	
	public String toString(){
		return name +" "+price;
	}
	
}
