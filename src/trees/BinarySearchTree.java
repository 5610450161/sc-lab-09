package trees;	

public class BinarySearchTree extends Node{
	  public BinarySearchTree(int value) {
		super(value);
		// TODO Auto-generated constructor stub
	}

	public Node root;

	  public void insert(int value){
	    Node node = new Node<>(value);

	    if ( root == null ) {
	      root = node;
	      return;
	    }

	    insertRec(root, node);

	  }

	  private void insertRec(Node latestRoot, Node node){

	    if ( latestRoot.value > node.value){

	      if ( latestRoot.left == null ){
	        latestRoot.left = node;
	        return;
	      }
	      else{
	        insertRec(latestRoot.left, node);
	      }
	    }
	    else{
	      if (latestRoot.right == null){
	        latestRoot.right = node;
	        return;
	      }
	      else{
	        insertRec(latestRoot.right, node);
	      }
	    }
	  }
	
	  public void printInorder(){
	    printInOrderRec(root);
	    System.out.println("");
	  }

	  private void printInOrderRec(Node currRoot){
	    if (currRoot == null){
	      return;
	    }
	    printInOrderRec(currRoot.left);
	    System.out.print(currRoot.value+", ");
	    printInOrderRec(currRoot.right);
	  }
	  
	  public void printPreorder() {
	    printPreOrderRec(root);
	    System.out.println("");
	  }

	  private void printPreOrderRec(Node currRoot) {
	    if (currRoot == null) {
	      return;
	    }
	    System.out.print(currRoot.value + ", ");
	    printPreOrderRec(currRoot.left);
	    printPreOrderRec(currRoot.right);
	  }
}

